//
//  SignupViewController.m
//
//  Created by Igor Medelyan on 21/05/16.
//

#import "SignupViewController.h"
#import "BounceAnimationController.h"
#import "RoundRectPresentationController.h"
#import "APIManager.h"
#import "Views/Popup/MessagePopupView.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Intercom/Intercom.h>
#import "TermsConfirmationPopupView.h"
#import "TermsTableViewController.h"

static NSString *const ErrorKey = @"com.alamofire.serialization.response.error.data";

@interface SignupViewController ()<UIViewControllerTransitioningDelegate>

@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword1;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword2;
@property (weak, nonatomic) IBOutlet UITextField *tfPhoneNumber;
@property (weak, nonatomic) IBOutlet UIImageView *emailOK;
@property (weak, nonatomic) IBOutlet UIImageView *passwordOK1;
@property (weak, nonatomic) IBOutlet UIImageView *passwordOK2;
@property (weak, nonatomic) IBOutlet UIImageView *phoneOK;
@property (weak, nonatomic) IBOutlet UIView *nameFieldView;
@property (weak, nonatomic) IBOutlet UIView *passwordFieldView1;
@property (weak, nonatomic) IBOutlet UIView *passwordFieldView2;
@property (weak, nonatomic) IBOutlet UIView *errorView;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (strong, nonatomic) UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIImageView *passwordIcon1;
@property (weak, nonatomic) IBOutlet UIImageView *passwordIcon2;
@property (weak, nonatomic) IBOutlet UIImageView *emailIcon;
@property (weak, nonatomic) IBOutlet UIButton *signInButton;
@property (weak, nonatomic) IBOutlet UILabel *alreadyAMamberLabel;

@end

TermsConfirmationPopupView *termsConfirmationPopup;

@implementation SignupViewController

#pragma mark - Initialization

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        if ([self respondsToSelector:@selector(setTransitioningDelegate:)]) {
            self.modalPresentationStyle = UIModalPresentationCustom;
            self.transitioningDelegate = self;
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.emailIcon.layer setMinificationFilter:kCAFilterTrilinear];
    [self.passwordIcon1.layer setMinificationFilter:kCAFilterTrilinear];
    [self.passwordIcon2.layer setMinificationFilter:kCAFilterTrilinear];
    
    self.signInButton.titleLabel.numberOfLines = 1;
    self.signInButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.signInButton.titleLabel.lineBreakMode = NSLineBreakByClipping;
    [self.alreadyAMamberLabel adjustsFontSizeToFitWidth];
    
    self.nameFieldView.layer.borderColor = [UIColor colorWithRed: 217.0f/255 green: 217.0f/255 blue: 217.0f/255 alpha: 1.0].CGColor;
    self.passwordFieldView1.layer.borderColor = [UIColor colorWithRed: 217.0f/255 green: 217.0f/255 blue: 217.0f/255 alpha: 1.0].CGColor;
    self.passwordFieldView2.layer.borderColor = [UIColor colorWithRed: 217.0f/255 green: 217.0f/255 blue: 217.0f/255 alpha: 1.0].CGColor;
    self.errorView.layer.borderColor = [UIColor colorWithRed: 236.0f/255 green: 164.0f/255 blue: 136.0f/255 alpha: 1.0].CGColor;
    self.registerButton.enabled = YES;
    
    [self.tfEmail addTarget:self action:@selector(emailFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.tfPassword1 addTarget:self action:@selector(password1FieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.tfPassword2 addTarget:self action:@selector(password2FieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.tfPhoneNumber addTarget:self action:@selector(phoneNumberFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self.view action:@selector(endEditing:)]];
    
    self.indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    self.indicator.center = CGPointMake([UIScreen mainScreen].bounds.size.width / 2, [UIScreen mainScreen].bounds.size.height / 2 - 80);
    self.indicator.transform = CGAffineTransformMakeScale(2.0, 2.0);
    [self.view addSubview:self.indicator];
    [self.indicator bringSubviewToFront:self.view];    
}

#pragma mark - Validation

- (BOOL)validate {
    if (!self.tfEmail.text.length) {
        self.errorLabel.text = NSLocalizedString(@"*Please enter your email address.", @"EnterEmail");
        self.errorView.hidden = NO;
        return NO;
    }
    if (![self validateEmailTextField:self.tfEmail]) {
        self.errorLabel.text = NSLocalizedString(@"*Please enter valid email address.", @"EnterValidEmail");
        self.errorView.hidden = NO;
        return NO;
    }
    if (!self.tfPassword1.text.length || !self.tfPassword2.text.length) {
        self.errorLabel.text = NSLocalizedString(@"*Please enter password.", @"Pass");
        self.errorView.hidden = NO;
        return NO;
    }
    if (![self validatePasswordsMatch]) {
        self.errorLabel.text = NSLocalizedString(@"*Your passwords do not match. Please try again.", @"PassNotMatch");
        self.errorView.hidden = NO;
        return NO;
    }
    if (self.tfPassword1.text.length < 6 || self.tfPassword2.text.length < 6) {
        self.errorLabel.text = NSLocalizedString(@"*Password can't be less than 6 characters.", @"*Password can't be less than 6 characters.");
        self.errorView.hidden = NO;
        return NO;
    }
    return YES;
}

- (BOOL)validateEmailTextField:(UITextField *)textField{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if (![emailTest evaluateWithObject:textField.text]) {
        return NO;
    }
    return YES;
}

- (BOOL)validatePasswordsMatch{
    NSString *password = self.tfPassword1.text;
    NSString *confirmPassword = self.tfPassword2.text;
    if(![password isEqualToString:confirmPassword]){
        return NO;
    }
    return YES;
}

- (BOOL)validatePhoneNumberTextField:(UITextField *)textField{
    NSString *numberRegEx = @"[0-9]{10}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if (![numberTest evaluateWithObject:textField.text]) {
        return NO;
    }
    return YES;
}

- (void)emailFieldEditingChanged:(id)sender {
    self.errorView.hidden = YES;
    self.tfEmail.textColor = [UIColor blackColor];
    self.emailIcon.image = [UIImage imageNamed:@"login_user_icon_black"];
    if ([self validateEmailTextField:self.tfEmail])
    {
        self.emailOK.hidden = NO;
    } else {
        self.emailOK.hidden = YES;
    }
}

- (void)password1FieldEditingChanged:(id)sender {
    self.errorView.hidden = YES;
    self.passwordIcon1.image = [UIImage imageNamed:@"login_pass_icon_black"];
    if (self.tfPassword1.text.length) {
        self.passwordOK1.hidden = NO;
    } else {
        self.passwordOK1.hidden = YES;
    }
}

- (void)password2FieldEditingChanged:(id)sender {
    self.errorView.hidden = YES;
    self.passwordIcon2.image = [UIImage imageNamed:@"login_pass_icon_black"];
    if ([self validatePasswordsMatch]) {
        self.passwordOK2.hidden = NO;
    } else {
        self.passwordOK2.hidden = YES;
    }
}

- (void)phoneNumberFieldEditingChanged:(id)sender {
    self.errorView.hidden = YES;
    if ([self validatePhoneNumberTextField:self.tfPhoneNumber])
    {
        self.phoneOK.hidden = NO;
    } else {
        self.phoneOK.hidden = YES;
    }
}

#pragma mark - Button actions

- (IBAction)Action_DoRegister:(id)sender {
    self.errorView.hidden = YES;
    self.errorView.backgroundColor = [UIColor colorWithRed:247.f/255.f green:207.f/255.f blue:171.f/255.f alpha:1.0f];
    self.errorLabel.textColor = [UIColor colorWithRed:166.f/255.f green:59.f/255.f blue:47.f/255.f alpha:1.0f];
    self.errorView.layer.borderColor = [UIColor colorWithRed:166.f/255.f green:59.f/255.f blue:47.f/255.f alpha:1.0f].CGColor;
    if (![self validate]) { return; }
    
    termsConfirmationPopup = [TermsConfirmationPopupView popup];
    termsConfirmationPopup.center = self.view.center;
    termsConfirmationPopup.delegate = self;
    [termsConfirmationPopup show];
}

- (void)showTerms {
    TermsTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsNavigationController"];
    [self presentViewController:vc animated:NO completion:nil];
}

- (void)doRegister {
    [termsConfirmationPopup hide];
    
    self.registerButton.enabled = NO;
    [self.indicator startAnimating];
    
    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    [df setObject:self.tfEmail.text forKey:@"user"];
    [UserService sharedInstance].profile.email = self.tfEmail.text;
    
    if ([[UserService sharedInstance] tokenExpireDate] != nil && [UserService sharedInstance].refreshToken != nil) {
        if ([[UserService sharedInstance] tokenExpired]) {
            [[APIManager sharedInstance] refreshToken:[UserService sharedInstance].refreshToken completion:^(id result, NSError *error) {
                if (!error) {
                    [[APIManager sharedInstance] updateTokenHeader];
                    [[UserService sharedInstance] scheduleNotificationForTokenExpired];
                    [self registerUser];
                    NSLog(@"Refresh Token success");
                } else {
                    NSLog(@"Refresh Token error");
                    [self registerUser];
                }
            }];
        } else {
            [self registerUser];
        }
    } else {
        [self registerUser];
    }
}

- (IBAction)Action_LoginFB:(UIButton *)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
                 fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"FB Login error");
         } else if (result.isCancelled) {
             NSLog(@"FB Login Cancelled");
         } else {
             NSLog(@"FB Logged in");
             
             if ([FBSDKAccessToken currentAccessToken]) {
                 [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
                  startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                      if (!error) {
                          NSLog(@"fetched user:%@", result);
                      }
                  }];
             }
             
             [[APIManager sharedInstance] updateTokenHeader];
             [UserService sharedInstance].token = [FBSDKAccessToken currentAccessToken].tokenString;
             [UserService sharedInstance].tokenExpireDate = [FBSDKAccessToken currentAccessToken].expirationDate;
             if (self.delegate) {
                 [self.delegate updateMap];
                 [self dismissViewControllerAnimated:NO completion:^{
                     [self.delegate updateLogInButton];
                 }];
             }
         }
     }];
}

- (void)showGettingStarted {
    static NSString* const hasRunAppOnceKey = @"hasRunAppOnceKey";
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"[defaults boolForKey:hasRunAppOnceKey]: %hhd",[defaults boolForKey:hasRunAppOnceKey]);
    if (![defaults boolForKey:hasRunAppOnceKey]) {
        [self performSegueWithIdentifier:@"showGettingStarted" sender:nil];
    } else {
        [self performSegueWithIdentifier:@"startSegue" sender:nil];
    }
}

- (IBAction)Action_DoSignIn:(id)sender {
    [self performSegueWithIdentifier:@"logSegue" sender:nil];
}

- (NSString *)parseErrorMessage:(NSError *)error{
    NSData *errorData = error.userInfo[ErrorKey];
    if(errorData){
        NSString *errorString = [[NSString alloc] initWithData:errorData encoding:NSUTF8StringEncoding];
        NSLog(@"error = %@", errorString);
        
        NSDictionary *validationResult = [NSJSONSerialization JSONObjectWithData:errorData options:NSJSONReadingMutableContainers error:nil];
        
        if(validationResult){
            if ([[validationResult valueForKey:@"message"] containsString:@"Email not confirmed!"])
            {
                self.errorView.backgroundColor = [UIColor colorWithRed:188.f/255.f green:240.f/255.f blue:218.f/255.f alpha:1.0f];
                self.errorLabel.textColor = [UIColor colorWithRed:52.f/255.f green:170.f/255.f blue:118.f/255.f alpha:1.0f];
                self.errorView.layer.borderColor = [UIColor colorWithRed:52.f/255.f green:170.f/255.f blue:118.f/255.f alpha:1.0f].CGColor;
                return NSLocalizedString(@"Please check your email", @"Please check your email and click the confirmation link. If you didn't get the email check your SPAM folder");
            }
            self.errorView.backgroundColor = [UIColor colorWithRed:247.f/255.f green:207.f/255.f blue:171.f/255.f alpha:1.0f];
            self.errorLabel.textColor = [UIColor colorWithRed:166.f/255.f green:59.f/255.f blue:47.f/255.f alpha:1.0f];
            self.errorView.layer.borderColor = [UIColor colorWithRed:166.f/255.f green:59.f/255.f blue:47.f/255.f alpha:1.0f].CGColor;
            
            return [validationResult valueForKey:@"message"];
        }
    }
    return nil;
}

#pragma mark - API requests

- (void)registerUser {
    [[APIManager sharedInstance] registerUser:self.tfEmail.text password:self.tfPassword1.text completion:^(id result, NSError *error) {
        if (!error) {
            [self.indicator stopAnimating];
            self.errorView.backgroundColor = [UIColor colorWithRed:188.f/255.f green:240.f/255.f blue:218.f/255.f alpha:1.0f];
            self.errorLabel.textColor = [UIColor colorWithRed:52.f/255.f green:170.f/255.f blue:118.f/255.f alpha:1.0f];
            self.errorView.layer.borderColor = [UIColor colorWithRed:52.f/255.f green:170.f/255.f blue:118.f/255.f alpha:1.0f].CGColor;
            self.errorLabel.text = NSLocalizedString(@"Please check your email", @"Please check your email and click the confirmation link. If you didn't get the email check your SPAM folder");
            self.errorView.hidden = NO;
            self.registerButton.enabled = YES;
        } else {
            [self.indicator stopAnimating];
            if ([self parseErrorMessage:error]) {
                self.errorLabel.text = [self parseErrorMessage:error];
            } else {
                self.errorLabel.text = NSLocalizedString(@"*Server respone Error.", @"*Server respone Error.");
            }
            self.errorView.hidden = NO;
            self.registerButton.enabled = YES;
        }
    }];
}

#pragma mark - UIViewControllerTransitioningDelegate

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    return [[BounceAnimationController alloc] init];
}

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
    return [[RoundRectPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting andRect:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
}

@end
