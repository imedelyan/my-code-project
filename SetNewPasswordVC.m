//
//  SetNewPasswordVC.m
//
//  Created by Igor Medelyan on 2/28/17.
//

#import "SetNewPasswordVC.h"
#import "BounceAnimationController.h"
#import "RoundRectPresentationController.h"
#import "APIManager.h"
#import "Views/Popup/MessagePopupView.h"

static NSString *const ErrorKey = @"com.alamofire.serialization.response.error.data";

@interface SetNewPasswordVC ()

@property (weak, nonatomic) IBOutlet UITextField *tfPassword1;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword2;
@property (weak, nonatomic) IBOutlet UIView *passwordFieldView1;
@property (weak, nonatomic) IBOutlet UIView *passwordFieldView2;
@property (weak, nonatomic) IBOutlet UIView *errorView;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIImageView *passwordIcon1;
@property (weak, nonatomic) IBOutlet UIImageView *passwordIcon2;
@property (weak, nonatomic) IBOutlet UIButton *signInButton;
@property (weak, nonatomic) IBOutlet UILabel *goBackLabel;

@end

@implementation SetNewPasswordVC

#pragma mark - Initialization

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        if ([self respondsToSelector:@selector(setTransitioningDelegate:)]) {
            self.modalPresentationStyle = UIModalPresentationCustom;
            self.transitioningDelegate = self;
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.passwordIcon1.layer setMinificationFilter:kCAFilterTrilinear];
    [self.passwordIcon2.layer setMinificationFilter:kCAFilterTrilinear];
    
    self.signInButton.titleLabel.numberOfLines = 1;
    self.signInButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.signInButton.titleLabel.lineBreakMode = NSLineBreakByClipping;
    [self.goBackLabel adjustsFontSizeToFitWidth];
    
    self.passwordFieldView1.layer.borderColor = [UIColor colorWithRed: 217.0f/255 green: 217.0f/255 blue: 217.0f/255 alpha: 1.0].CGColor;
    self.passwordFieldView2.layer.borderColor = [UIColor colorWithRed: 217.0f/255 green: 217.0f/255 blue: 217.0f/255 alpha: 1.0].CGColor;
    self.errorView.layer.borderColor = [UIColor colorWithRed: 236.0f/255 green: 164.0f/255 blue: 136.0f/255 alpha: 1.0].CGColor;
    self.saveButton.enabled = YES;
    
    [self.tfPassword1 addTarget:self action:@selector(password1FieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.tfPassword2 addTarget:self action:@selector(password2FieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self.view action:@selector(endEditing:)]];
}

#pragma mark - Validation

- (BOOL)validate {
    if (!self.tfPassword1.text.length || !self.tfPassword2.text.length) {
        self.errorLabel.text = NSLocalizedString(@"*Please enter password.", @"Pass");
        self.errorView.hidden = NO;
        return NO;
    }
    if (![self validatePasswordsMatch]) {
        self.errorLabel.text = NSLocalizedString(@"*Your passwords do not match. Please try again.", @"PassNotMatch");
        self.errorView.hidden = NO;
        return NO;
    }
    if (self.tfPassword1.text.length < 6 || self.tfPassword2.text.length < 6) {
        self.errorLabel.text = NSLocalizedString(@"*Password can't be less than 6 characters.", @"*Password can't be less than 6 characters.");
        self.errorView.hidden = NO;
        return NO;
    }
    return YES;
}

- (BOOL)validatePasswordsMatch{
    NSString *password = self.tfPassword1.text;
    NSString *confirmPassword = self.tfPassword2.text;
    if(![password isEqualToString:confirmPassword]){
        return NO;
    }
    return YES;
}

-(void) password1FieldEditingChanged:(id)sender {
    self.errorView.hidden = YES;
    self.passwordIcon1.image = [UIImage imageNamed:@"login_pass_icon_black"];
}

-(void) password2FieldEditingChanged:(id)sender {
    self.errorView.hidden = YES;
    self.passwordIcon2.image = [UIImage imageNamed:@"login_pass_icon_black"];
}

- (NSString *)parseErrorMessage:(NSError *)error{
    NSData *errorData = error.userInfo[ErrorKey];
    if(errorData){
        NSString *errorString = [[NSString alloc] initWithData:errorData encoding:NSUTF8StringEncoding];
        NSLog(@"error = %@", errorString);
        
        NSDictionary *validationResult = [NSJSONSerialization JSONObjectWithData:errorData options:NSJSONReadingMutableContainers error:nil];
        
        if(validationResult){
            return [validationResult valueForKey:@"message"];
        }
    }
    return nil;
}

#pragma mark - Button actions

- (IBAction)Action_DoSave:(id)sender {
    if (![self validate])
    {
        return;
    }
    self.saveButton.enabled = NO;
    NSString *password = self.tfPassword1.text;
    [[APIManager sharedInstance] setNewPasswordForUser:password completion:^(id result, NSError *error) {
        if (!error) {
            [[MessagePopupView popupWithTitle:NSLocalizedString(@"Password changed.", @"Password changed")] show];
            [self dismissViewControllerAnimated:YES completion:^{
                if (self.delegate)
                    [self.delegate showLogin];
            }];
        } else {
            if ([self parseErrorMessage:error]) {
                self.errorLabel.text = [self parseErrorMessage:error];
            } else {
                self.errorLabel.text = NSLocalizedString(@"*Server respone Error.", @"*Server respone Error.");
            }
            self.errorView.hidden = NO;
            self.saveButton.enabled = YES;
        }
    }];
}

- (IBAction)Action_DoSignIn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate showLogin];
    }];
}

#pragma mark - UIViewControllerTransitioningDelegate

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    return [[BounceAnimationController alloc] init];
}

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
    return [[RoundRectPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting andRect:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
}

@end
