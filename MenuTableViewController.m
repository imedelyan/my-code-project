//
//  MenuTableViewController.m
//
//  Created by Igor Medelyan on 12/15/16.
//

#import "MenuTableViewController.h"
#import "LoginPVC.h"
#import <REFrostedViewController.h>
#import "EditProfileViewController.h"
#import "AboutTableViewController.h"
#import "ContactUsTableViewController.h"
#import "LanguagesTableViewController.h"
#import "UserService.h"
#import "SubscriptionDataModel.h"
#import "ProfileModel.h"
#import "UserInfoView.h"

@interface MenuTableViewController () <UIViewControllerTransitioningDelegate>

@property (weak, nonatomic) IBOutlet UILabel *userEmailLbl;
@property (weak, nonatomic) IBOutlet UILabel *subscriptionLbl;
@property (strong, nonatomic) IBOutlet UILabel *languageLbl;

@property (weak, nonatomic) IBOutlet UISwitch *switchFS;
@property (weak, nonatomic) IBOutlet UISwitch *switchTP;
@property (weak, nonatomic) IBOutlet UISwitch *switchCS;
@property (weak, nonatomic) IBOutlet UISwitch *switchAC;
@property (weak, nonatomic) IBOutlet UISwitch *switchCW;

@property (weak, nonatomic) NSString *email;
@property (weak, nonatomic) NSString *subscriptionType;
@property (weak, nonatomic) NSString *subscriptionStatus;
@property (weak, nonatomic) NSString *subscribeButton;

@property (strong, nonatomic) UIActivityIndicatorView *indicator;
@property (strong, nonatomic) IBOutlet UserInfoView *userInfo;

@property (weak, nonatomic) IBOutlet UILabel *getFreePremiumLabel;
@property (weak, nonatomic) IBOutlet UILabel *trackingProtectionLabel;
@property (weak, nonatomic) IBOutlet UILabel *aboutLabel;
@property (weak, nonatomic) IBOutlet UILabel *subscribeLabel;
@property (weak, nonatomic) IBOutlet UILabel *languageLabel;
@property (weak, nonatomic) IBOutlet UILabel *autoConnectLabel;
@property (weak, nonatomic) IBOutlet UILabel *connectOnWiFiLabel;
@property (weak, nonatomic) IBOutlet UILabel *feelSecureLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactUsLabel;
@property (weak, nonatomic) IBOutlet UILabel *rateUsLabel;

@property (weak, nonatomic) IBOutlet UIButton *editProfileButton;

@property (weak, nonatomic) IBOutlet UIImageView *aboutIcon;
@property (weak, nonatomic) IBOutlet UIImageView *subscribeIcon;
@property (weak, nonatomic) IBOutlet UIImageView *getFreeIcon;
@property (weak, nonatomic) IBOutlet UIImageView *languageIcon;
@property (weak, nonatomic) IBOutlet UIImageView *autoConnectIcon;
@property (weak, nonatomic) IBOutlet UIImageView *connectOnWiFiIcon;
@property (weak, nonatomic) IBOutlet UIImageView *feelSecureIcon;
@property (weak, nonatomic) IBOutlet UIImageView *protectionIcon;
@property (weak, nonatomic) IBOutlet UIImageView *contactUsIcon;
@property (weak, nonatomic) IBOutlet UIImageView *rateUsIcon;
@property (weak, nonatomic) IBOutlet UIImageView *userIcon;

@end

@implementation MenuTableViewController

#define APP_STORE_ID 123456789

#pragma mark - Setup

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.tableView setContentOffset:CGPointZero animated:YES];
    [self.userInfo updateInfo];
    [self setUpLanguage];
    [self setUpSwitches];
    [self setUpFonts];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setUpLanguage {
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:language];
    NSString *languageCode = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
    
    NSLocale *enLocale = [[NSLocale alloc] initWithLocaleIdentifier:language];
    NSString *languageName = [enLocale displayNameForKey:NSLocaleIdentifier value:languageCode]; //name of language
    
    self.languageLbl.text = languageName;
}

- (void)setUpFonts {
    [self.getFreePremiumLabel setAdjustsFontSizeToFitWidth:YES];
    [self.trackingProtectionLabel setAdjustsFontSizeToFitWidth:YES];
    [self.aboutLabel setAdjustsFontSizeToFitWidth:YES];
    [self.subscribeLabel setAdjustsFontSizeToFitWidth:YES];
    [self.languageLabel setAdjustsFontSizeToFitWidth:YES];
    [self.autoConnectLabel setAdjustsFontSizeToFitWidth:YES];
    [self.connectOnWiFiLabel setAdjustsFontSizeToFitWidth:YES];
    [self.feelSecureLabel setAdjustsFontSizeToFitWidth:YES];
    [self.contactUsLabel setAdjustsFontSizeToFitWidth:YES];
    [self.rateUsLabel setAdjustsFontSizeToFitWidth:YES];
}

- (void)setUpSwitches {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:[NSString stringWithFormat:@"SwitchFSState_%@", [UserService sharedInstance].profile.email]]) {
        self.switchFS.on = [defaults boolForKey:[NSString stringWithFormat:@"SwitchFSState_%@", [UserService sharedInstance].profile.email]];
    }
    if ([defaults objectForKey:[NSString stringWithFormat:@"SwitchTPState_%@", [UserService sharedInstance].profile.email]]) {
        self.switchTP.on = [defaults boolForKey:[NSString stringWithFormat:@"SwitchTPState_%@", [UserService sharedInstance].profile.email]];
    }
    if ([defaults objectForKey:[NSString stringWithFormat:@"SwitchCSState_%@", [UserService sharedInstance].profile.email]]) {
        self.switchCS.on = [defaults boolForKey:[NSString stringWithFormat:@"SwitchCSState_%@", [UserService sharedInstance].profile.email]];
    }
    if ([defaults objectForKey:[NSString stringWithFormat:@"SwitchACState_%@", [UserService sharedInstance].profile.email]]) {
        self.switchAC.on = [defaults boolForKey:[NSString stringWithFormat:@"SwitchACState_%@", [UserService sharedInstance].profile.email]];
    }
    if ([defaults objectForKey:[NSString stringWithFormat:@"SwitchCWState_%@", [UserService sharedInstance].profile.email]]) {
        self.switchCW.on = [defaults boolForKey:[NSString stringWithFormat:@"SwitchCWState_%@", [UserService sharedInstance].profile.email]];
    }
}

- (void)setUpView {
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    //Setup icons
    [self.aboutIcon.layer setMinificationFilter:kCAFilterTrilinear];
    [self.subscribeIcon.layer setMinificationFilter:kCAFilterTrilinear];
    [self.getFreeIcon.layer setMinificationFilter:kCAFilterTrilinear];
    [self.languageIcon.layer setMinificationFilter:kCAFilterTrilinear];
    [self.autoConnectIcon.layer setMinificationFilter:kCAFilterTrilinear];
    [self.connectOnWiFiIcon.layer setMinificationFilter:kCAFilterTrilinear];
    [self.feelSecureIcon.layer setMinificationFilter:kCAFilterTrilinear];
    [self.protectionIcon.layer setMinificationFilter:kCAFilterTrilinear];
    [self.contactUsIcon.layer setMinificationFilter:kCAFilterTrilinear];
    [self.rateUsIcon.layer setMinificationFilter:kCAFilterTrilinear];
    [self.userIcon.layer setMinificationFilter:kCAFilterTrilinear];
    
    //Setup switches
    self.switchTP.transform = CGAffineTransformMakeScale(0.7, 0.7);
    self.switchFS.transform = CGAffineTransformMakeScale(0.7, 0.7);
    self.switchCS.transform = CGAffineTransformMakeScale(0.7, 0.7);
    self.switchAC.transform = CGAffineTransformMakeScale(0.7, 0.7);
    self.switchCW.transform = CGAffineTransformMakeScale(0.7, 0.7);
    
    [self setBackButton];
    
    //Setup activity indicator
    self.indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    self.indicator.center = CGPointMake([UIScreen mainScreen].bounds.size.width / 2, [UIScreen mainScreen].bounds.size.height / 2 - 80);
    self.indicator.transform = CGAffineTransformMakeScale(2.0, 2.0);
    [self.view addSubview:self.indicator];
    [self.indicator bringSubviewToFront:self.view];
    
    //Setup labels
    self.autoConnectLabel.text = NSLocalizedString(@"Auto connect", @"Auto connect");
    self.connectOnWiFiLabel.text = NSLocalizedString(@"Connect on wifi", @"Connect on wifi");
    self.rateUsLabel.text = NSLocalizedString(@"Rate us", @"Rate us");
    
    //Setup FrostedViewController
    self.frostedViewController.limitMenuViewSize = YES;
    [self.frostedViewController setMenuViewSize: CGSizeMake([UIScreen mainScreen].bounds.size.width - 50.0f, [UIScreen mainScreen].bounds.size.height)];
    [self.frostedViewController resizeMenuViewControllerToSize: CGSizeMake([UIScreen mainScreen].bounds.size.width - 50.0f, [UIScreen mainScreen].bounds.size.height)];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

- (void)setBackButton {
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:language];
    NSString *languageCode = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
    NSString *currentLang = [languageCode substringWithRange:NSMakeRange(0, 2)];
    
    UIBarButtonItem *item0 = [[UIBarButtonItem alloc]
                              initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                              target:nil
                              action:nil];
    item0.width = -8;
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 0);
    UIImage *alignedImage = [UIImage new];
    if ([currentLang isEqualToString:@"ar"] || [currentLang isEqualToString:@"fa"]) {
        alignedImage = [[UIImage imageNamed:@"back_button_icon_white_ar"] imageWithAlignmentRectInsets:insets];
    } else {
        alignedImage = [[UIImage imageNamed:@"back_button_icon_white"] imageWithAlignmentRectInsets:insets];
    }
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithImage:alignedImage
                                                              style:UIBarButtonItemStylePlain
                                                             target:self
                                                             action:@selector(actionClose:)];
    NSArray* items = @[item0, item1];
    [self.navigationItem setLeftBarButtonItems:items animated:NO];
    [self.navigationItem setLeftItemsSupplementBackButton:YES];
}

- (void)setupSubscriptionType: (NSString *)text {
    if ([text isEqualToString:@"1 Year"]) {
        self.subscriptionType = NSLocalizedString(@"1 Year", @"1 Year");
    } else if ([text isEqualToString:@"6 Months"]) {
        self.subscriptionType = NSLocalizedString(@"6 Months", @"6 Months");
    } else {
        self.subscriptionType = NSLocalizedString(@"1 Month", @"1 Month");
    }
}

#pragma mark - Menu Button Actions

- (IBAction)actionLogOut:(UIButton *)sender {
    self.delegate = self.frostedViewController.delegate;
    [self.delegate logOut];
}

- (IBAction)actionClose:(id)sender {
    [self.frostedViewController hideMenuViewController];
}

- (IBAction)actionEditProfile:(UIButton *)sender {
    self.editProfileButton.enabled = NO;
    [self.indicator startAnimating];
    
    if ([[UserService sharedInstance] isValidToken]) {
        self.subscribeButton = NSLocalizedString(@"Unsubscribe", @"Unsubscribe");
        if ([UserService sharedInstance].profile.email.length != 0) {
            self.email = [UserService sharedInstance].profile.email;
        } else {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            self.email = [defaults objectForKey: @"user"];
        }
        
        NSString *subName = [[UserService sharedInstance] getUserSubscription];
        if (!subName) {
            subName = @"1 Month";
        }
        [self setupSubscriptionType:subName];
        
        if ([self.subscriptionType isEqualToString:NSLocalizedString(@"1 Month", @"1 Month")]) {
            self.subscribeButton = NSLocalizedString(@"Upgrade", @"Upgrade");
        }
        
        NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
        NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:[df doubleForKey:@"SubscriptionEndDate"]/1000];
        if([endDate compare: [NSDate date]] == NSOrderedDescending) {// if endDate is later in time than now
            self.subscriptionStatus = NSLocalizedString(@"Active", @"Active");
        } else {
            self.subscriptionStatus = NSLocalizedString(@"Expired", @"Expired");
            self.subscribeButton = NSLocalizedString(@"Upgrade", @"Upgrade");
        }
        
        [self.indicator stopAnimating];
        self.editProfileButton.enabled = YES;
        [self.frostedViewController resizeMenuViewControllerToSize: [UIScreen mainScreen].bounds.size];
        [self performSegueWithIdentifier:@"showEditProfile" sender:nil];
    } else {
        [self.indicator stopAnimating];
        self.editProfileButton.enabled = YES;
    }
}

- (void)showSubscribe {
    [self.frostedViewController resizeMenuViewControllerToSize: [UIScreen mainScreen].bounds.size];
    [self performSegueWithIdentifier:@"showSubscribe" sender:nil];
}

- (IBAction)actionSwitchAC:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([self.switchAC isOn]) {
        [defaults setBool:YES forKey:[NSString stringWithFormat:@"SwitchACState_%@", [UserService sharedInstance].profile.email]];
    } else {
        [defaults setBool:NO forKey:[NSString stringWithFormat:@"SwitchACState_%@", [UserService sharedInstance].profile.email]];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)actionSwitchCW:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([self.switchCW isOn]) {
        [defaults setBool:YES forKey:[NSString stringWithFormat:@"SwitchCWState_%@", [UserService sharedInstance].profile.email]];
    } else {
        [defaults setBool:NO forKey:[NSString stringWithFormat:@"SwitchCWState_%@", [UserService sharedInstance].profile.email]];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)actionSwitchFS:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([self.switchFS isOn]) {
        [defaults setBool:YES forKey:[NSString stringWithFormat:@"SwitchFSState_%@", [UserService sharedInstance].profile.email]];
    } else {
        [defaults setBool:NO forKey:[NSString stringWithFormat:@"SwitchFSState_%@", [UserService sharedInstance].profile.email]];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)actionSwitchTP:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([self.switchTP isOn]) {
        [defaults setBool:YES forKey:[NSString stringWithFormat:@"SwitchTPState_%@", [UserService sharedInstance].profile.email]];
    } else {
        [defaults setBool:NO forKey:[NSString stringWithFormat:@"SwitchTPState_%@", [UserService sharedInstance].profile.email]];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)actionSwitchCS:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([self.switchCS isOn]) {
        [defaults setBool:YES forKey:[NSString stringWithFormat:@"SwitchCSState_%@", [UserService sharedInstance].profile.email]];
    } else {
        [defaults setBool:NO forKey:[NSString stringWithFormat:@"SwitchCSState_%@", [UserService sharedInstance].profile.email]];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showEditProfile"]) {
        EditProfileViewController *vc = [segue destinationViewController];
        vc.email = self.email;
        vc.subscribeButtonTitle = self.subscribeButton;
        vc.subscriptionType = self.subscriptionType;
        vc.subscriptionStatus = self.subscriptionStatus;
        vc.delegate = self;
    }
    if ([[segue identifier] isEqualToString:@"showAbout"]) {
        AboutTableViewController *vc = [segue destinationViewController];
        vc.delegate = self;
    }
    if ([[segue identifier] isEqualToString:@"showContactUs"]) {
        ContactUsTableViewController *vc = [segue destinationViewController];
        vc.delegate = self;
    }
    if ([[segue identifier] isEqualToString:@"showLanguages"]) {
        LanguagesTableViewController *vc = [segue destinationViewController];
        vc.delegate = self;
    }
}

#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UITableViewCell *aboutCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    UITableViewCell *subscribeCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    UITableViewCell *getFreeCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    UITableViewCell *languagesCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    UITableViewCell *contactUsCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    UITableViewCell *rateUsCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2]];
    UITableViewCell *logoutCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:3]];
    
    if (cell == subscribeCell) {
        [self showSubscribe];
    } else if (cell == getFreeCell) {
        [self.frostedViewController resizeMenuViewControllerToSize: [UIScreen mainScreen].bounds.size];
        [self performSegueWithIdentifier:@"showGetFree" sender:nil];
    } else if (cell == languagesCell) {
        [self performSegueWithIdentifier:@"showLanguages" sender:nil];
    } else if (cell == contactUsCell) {
        [self performSegueWithIdentifier:@"showContactUs" sender:nil];
    } else if (cell == aboutCell) {
        [self performSegueWithIdentifier:@"showAbout" sender:nil];
    } else if (cell == rateUsCell) {
        static NSString *const iOS7AppStoreURLFormat = @"itms-apps://itunes.apple.com/app/id%d";
        static NSString *const iOSAppStoreURLFormat = @"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%d";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:([[UIDevice currentDevice].systemVersion floatValue] >= 7.0f)? iOS7AppStoreURLFormat: iOSAppStoreURLFormat, APP_STORE_ID]]];
    } else if (cell == logoutCell) {
        [self actionLogOut:nil];
    }
}

@end
