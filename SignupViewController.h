//
//  SignupViewController.h
//
//  Created by Igor Medelyan on 21/05/16.
//

#import <UIKit/UIKit.h>

@protocol SignupViewControllerDelegate <NSObject>

- (void)showLogin;
- (void)updateMap;
- (void)updateLogInButton;
- (void)showGettingStarted;
- (void)showTermsAndConditions;

@property (weak, nonatomic) IBOutlet UIButton *logInButton;

@end

@interface SignupViewController : UIViewController

@property (weak, nonatomic) id<SignupViewControllerDelegate> delegate;

@end
