//
//  ConnectButton.m
//
//  Created by Igor Medelyan on 5/8/17.
//

#import "ConnectButton.h"

@implementation ConnectButton

#pragma mark­ - Initialization

- (void)awakeFromNib {
    [super awakeFromNib];
    self.layer.borderColor = [self colorFromHexString:@"#3B7CCA"].CGColor;
    self.layer.borderWidth = 2.f;
    self.layer.cornerRadius = self.frame.size.width / 2.f;
}

- (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

#pragma mark­ - Animation

- (void)flashConnectButton {
    CABasicAnimation *flashAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    flashAnimation.fromValue = [NSNumber numberWithFloat:1.0];
    flashAnimation.toValue = [NSNumber numberWithFloat:0.0];
    flashAnimation.duration = 0.3;
    flashAnimation.repeatCount = 3;
    flashAnimation.removedOnCompletion = NO;
    [self.layer addAnimation:flashAnimation forKey:@"flashAnimation"];
}

- (void)showConnectButtonRotationAnimation {
    CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
    animationGroup.duration = 1;
    animationGroup.repeatCount = INFINITY;
    
    CAMediaTimingFunction *easeOut = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    CABasicAnimation* animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.fromValue = [NSNumber numberWithFloat:0.0f];
    animation.toValue = [NSNumber numberWithFloat: -2*M_PI];
    animation.duration = 0.8f;
    animation.repeatCount = 1;
    animation.timingFunction = easeOut;
    
    animationGroup.animations = @[animation];
    animationGroup.removedOnCompletion = NO;
    
    [self.layer addAnimation:animationGroup forKey:@"rotation"];
}

- (void)removeConnectButtonRotationAnimation {
    [self.layer removeAnimationForKey:@"rotation"];
}

@end
