//
//  UserService.h
//
//  Created by Igor Medelyan on 8/29/16.
//

#import <Foundation/Foundation.h>
#import "ServerDataModel.h"
#import "ProfileModel.h"

@interface UserService : NSObject

+ (ServerDataModel*)getSelectedItem;
+ (void)setSelectedItem:(ServerDataModel*)item;
+ (NSString*)getSelectedProtocol;
+ (void)setSelectedProtocol:(NSString*)item;
+ (instancetype)sharedInstance;
+ (void)clearSharedInstance;

@property (nonatomic,assign) NSDate *tokenExpireDate;
@property (nonatomic,assign) NSString *token;
@property (nonatomic,assign) NSString *refreshToken;
@property (nonatomic,assign) NSString *intHash;
@property (nonatomic,strong) NSMutableArray *listOfServers;
@property (nonatomic,strong) ProfileModel *profile;
@property (nonatomic,strong) NSMutableArray *subscriptions;

- (BOOL)isValidToken;
- (void)tokenClear;
- (void)saveNewSubscription:(NSString*)subscriptionId;
- (void)clearSubscription;
- (NSString*)getUserSubscription;
- (BOOL)tokenExpired;
- (void)scheduleNotificationForTokenExpired;
- (BOOL)subscriptionExpired;
- (void)scheduleNotificationForSubscriptionExpired;

@end
