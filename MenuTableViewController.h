//
//  MenuTableViewController.h
//
//  Created by Igor Medelyan on 12/15/16.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"

@protocol MenuTableViewControllerDelegate <NSObject>

- (void)updateMap;
- (void)showLogin;
- (void)logOut;
- (void)showEditProfile;

@end

@interface MenuTableViewController: UITableViewController

@property (weak, nonatomic) id <MenuTableViewControllerDelegate> delegate;

@end
