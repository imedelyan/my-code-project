//
//  UserInfoView.h
//
//  Created by Igor Medelyan on 3/21/17.
//

#import <UIKit/UIKit.h>

@interface UserInfoView : UIView

@property (nonatomic, strong) IBOutlet UILabel *userEmail;
@property (nonatomic, strong) IBOutlet UILabel *subscriptionDays;
@property (nonatomic, strong) IBOutlet UIImageView *profileIcon;
@property (nonatomic, strong) IBOutlet UIButton *editBtn;

- (void)updateInfo;

@end
