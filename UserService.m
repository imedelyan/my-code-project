//
//  UserService.m
//
//  Created by Igor Medelyan on 8/29/16.
//

#import "UserService.h"
#import "APIManager.h"
#import <Intercom/Intercom.h>

static NSString *const KeyIntHash = @"inthash";
static NSString *const KeyUserToken = @"KeyUserToken";
static NSString *const KeyUserTokenExpired = @"KeyUserTokenExpired";
static NSString *const KeyUserRefreshToken = @"KeyUserRefreshToken";
static NSString *const KeyUserSubscription = @"KeyUserSubscription";

@implementation UserService

#pragma mark­ - Initialization

static UserService *service = nil;
static ServerDataModel *selectedServer;
static NSString *selectedProtocol;

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [[self class]new];
    });
    return service;
}

+ (void)clearSharedInstance {
    service = nil;
}

#pragma mark­ - Server model and protocol

+ (ServerDataModel*)getSelectedItem{
    return selectedServer;
}

+ (void)setSelectedItem:(ServerDataModel*)item{
    selectedServer = item;
}

+ (NSString*)getSelectedProtocol {
    return selectedProtocol;
}

+ (void)setSelectedProtocol:(NSString*)item {
    selectedProtocol = item;
}

#pragma mark­ - Token

- (void)tokenClear {
    [self setToken:nil];
    [self setTokenExpireDate:nil];
}

- (NSDate*)tokenExpireDate {
    NSString *user = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    return [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@_%@",KeyUserTokenExpired, user]];
}

- (void)setTokenExpireDate:(NSDate*)date {
    NSString *user = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    if (!date) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"%@_%@",KeyUserTokenExpired, user]];
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:date forKey:[NSString stringWithFormat:@"%@_%@",KeyUserTokenExpired, user]];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString*)intHash {
    NSString *user = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    return [[NSUserDefaults standardUserDefaults] stringForKey:[NSString stringWithFormat:@"%@_%@",KeyIntHash, user]];
}

- (void)setIntHash:(NSString *)intHash {
    NSString *user = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    if (!intHash.length)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"%@_%@",KeyIntHash, user]];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:@([intHash UTF8String]) forKey:[NSString stringWithFormat:@"%@_%@",KeyIntHash, user]];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString*)token {
    NSString *user = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    return [[NSUserDefaults standardUserDefaults] stringForKey:[NSString stringWithFormat:@"%@_%@",KeyUserToken, user]];
}

- (void)setToken:(NSString*)token {
    NSString *user = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    if (!token.length) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"%@_%@",KeyUserToken, user]];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:@([token UTF8String]) forKey:[NSString stringWithFormat:@"%@_%@",KeyUserToken, user]];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSString*)refreshToken {
    NSString *user = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    return [[NSUserDefaults standardUserDefaults] stringForKey:[NSString stringWithFormat:@"%@_%@",KeyUserRefreshToken, user]];
}

- (void)setRefreshToken:(NSString*)refreshToken {
    NSString *user = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
    if (!refreshToken.length) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"%@_%@",KeyUserRefreshToken, user]];
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:@([refreshToken UTF8String]) forKey:[NSString stringWithFormat:@"%@_%@",KeyUserRefreshToken, user]];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)scheduleNotificationForTokenExpired {
    UILocalNotification *notif = [[UILocalNotification alloc] init];
    notif.fireDate = [self tokenExpireDate];
    notif.timeZone = [NSTimeZone systemTimeZone];
    NSDictionary *userDict = [NSDictionary dictionaryWithObject:@"TokenExpired" forKey:@"Notification"];
    notif.userInfo = userDict;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notif];
}

- (BOOL)isValidToken{
    if (![self token].length) {
        return NO;
    }
    return YES;
}

- (BOOL)tokenExpired {
    if ([self isValidToken]) {
        if ([[self tokenExpireDate] compare: [NSDate date]] == NSOrderedDescending) {
            return NO;
        } else
            return YES;
    } else
        return YES;
}

#pragma mark­ - Subscription

- (BOOL)subscriptionExpired {
    if ([self isValidToken]) {
        NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
        NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:[df doubleForKey:@"SubscriptionEndDate"]/1000];
        if ([endDate compare: [NSDate date]] == NSOrderedDescending) {
            return NO;
        } else
            return YES;
    } else
        return YES;
}

- (void)scheduleNotificationForSubscriptionExpired {
    if ([self isValidToken]) {
        NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
        NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:[df doubleForKey:@"SubscriptionEndDate"]/1000];
        
        UILocalNotification *notif = [[UILocalNotification alloc] init];
        notif.fireDate = endDate;
        notif.timeZone = [NSTimeZone systemTimeZone];
        NSDictionary *userDict = [NSDictionary dictionaryWithObject:@"SubscriptionExpired" forKey:@"Notification"];
        notif.userInfo = userDict;
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notif];
    }
}

- (void)saveNewSubscription:(NSString*)subscriptionId
{
    [[NSUserDefaults standardUserDefaults] setObject:subscriptionId forKey:[NSString stringWithFormat:@"%@_%@",KeyUserSubscription, self.profile.profileId]];
}

- (void)clearSubscription
{
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:[NSString stringWithFormat:@"%@_%@",KeyUserSubscription, self.profile.profileId]];
}

- (NSString*)getUserSubscription
{
    NSString *result = @"";
    if ([[NSUserDefaults standardUserDefaults] stringForKey:[NSString stringWithFormat:@"%@_%@",KeyUserSubscription, self.profile.profileId]].length != 0)
    {
        result = [[NSUserDefaults standardUserDefaults] stringForKey:[NSString stringWithFormat:@"%@_%@",KeyUserSubscription, self.profile.profileId]];
    } else {
        [self saveNewSubscription:@"1 Month"];
        result = @"1 Month";
    }
    return result;
}

@end
