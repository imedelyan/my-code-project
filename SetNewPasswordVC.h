//
//  SetNewPasswordVC.h
//
//  Created by Igor Medelyan on 2/28/17.
//

#import <UIKit/UIKit.h>

@protocol SetNewPasswordVCDelegate <NSObject>

-(void) showLogin;
-(void) updateMap;
-(void) updateLogInButton;

@property (weak, nonatomic) IBOutlet UIButton *logInButton;

@end

@interface SetNewPasswordVC : UIViewController

@property (weak, nonatomic) id <SetNewPasswordVCDelegate> delegate;

@end
