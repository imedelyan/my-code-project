//
//  EarthAnimationView.h
//
//  Created by Igor Medelyan on 1/12/17.
//

#import <UIKit/UIKit.h>

@interface EarthAnimationView : UIView

- (void)clearAnimation;
- (void)hideCasper;
- (void)flashCircle;
- (void)showEarthAnimation;

@property BOOL drawAnimationView;
@property UIImageView *casper;
@property CAShapeLayer *circle;

@end
