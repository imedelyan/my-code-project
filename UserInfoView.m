//
//  UserInfoView.m
//
//  Created by Igor Medelyan on 3/21/17.
//

#import "UserInfoView.h"
#import "UserService.h"
#import "APIManager.h"

@implementation UserInfoView

#pragma mark­ - Setup

- (void)updateInfo {
    [self setEditButton];
    if ([[UserService sharedInstance] isValidToken])
    {
        if ([UserService sharedInstance].profile.email.length != 0) {
            self.userEmail.text = [UserService sharedInstance].profile.email;
            
            NSString *subName = [[UserService sharedInstance] getUserSubscription];
            if (!subName) {
                subName = @"1 Month";
            }
            [self setSubscriptionTypeLabel:subName];
            
            self.editBtn.enabled = YES;
            [self refreshToken];
        } else {
            if ([[UserService sharedInstance] tokenExpired]) {
                [[APIManager sharedInstance] refreshToken:[UserService sharedInstance].refreshToken completion:^(id result, NSError *error) {
                    if (!error) {
                        [[APIManager sharedInstance] updateTokenHeader];
                        [[UserService sharedInstance] scheduleNotificationForTokenExpired];
                        NSLog(@"Refresh Token success");
                        [self getProfile];
                    } else {
                        NSLog(@"Refresh Token error");
                        [self getProfile];
                    }
                }];
            } else {
                [self getProfile];
            }
        }
    } else {
        self.editBtn.enabled = NO;
    }
}

- (void)setSubscriptionTypeLabel: (NSString *)text {
    if ([text isEqualToString:@"1 Year"]) {
        self.subscriptionDays.text = NSLocalizedString(@"1 Year", @"1 Year");
    } else if ([text isEqualToString:@"6 Months"]) {
        self.subscriptionDays.text = NSLocalizedString(@"6 Months", @"6 Months");
    } else {
        self.subscriptionDays.text = NSLocalizedString(@"1 Month", @"1 Month");
    }
}

- (void)setEditButton {
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSDictionary *languageDic = [NSLocale componentsFromLocaleIdentifier:language];
    NSString *languageCode = [languageDic objectForKey:@"kCFLocaleLanguageCodeKey"];
    NSString *currentLang = [languageCode substringWithRange:NSMakeRange(0, 2)];
    
    if ([currentLang isEqualToString:@"ar"] || [currentLang isEqualToString:@"fa"]) {
        [self.editBtn setBackgroundImage:[UIImage imageNamed:@"menu_edit_icon_rtl"] forState:UIControlStateNormal];
    } else {
        [self.editBtn setBackgroundImage:[UIImage imageNamed:@"menu_edit_icon"] forState:UIControlStateNormal];
    }
}

#pragma mark­ - API requests

- (void)refreshToken {
    if ([[UserService sharedInstance] tokenExpired]) {
        [[APIManager sharedInstance] refreshToken:[UserService sharedInstance].refreshToken completion:^(id result, NSError *error) {
            if (!error) {
                [[APIManager sharedInstance] updateTokenHeader];
                [[UserService sharedInstance] scheduleNotificationForTokenExpired];
                NSLog(@"Refresh Token success");
            } else {
                NSLog(@"Refresh Token error");
            }
        }];
    }
}

- (void)getProfile {
    [[APIManager sharedInstance] getProfile:^(id result, NSError *error) {
        if (!error) {
            ProfileModel *profile = [[ProfileModel alloc] init];
            profile.email = [[result objectForKey:@"data"] objectForKey:@"login"];
            SubscriptionDataModel *subscription = [[SubscriptionDataModel alloc] init];
            [subscription parseForUser:[[result objectForKey:@"data"] objectForKey:@"subscription"]];
            profile.subscription = subscription;
            SubscriptionDataDataModel *subscriptionData = [[SubscriptionDataDataModel alloc] init];
            [subscriptionData parseData:[[result objectForKey:@"data"] objectForKey:@"subscriptionData"]];
            profile.subscriptionData = subscriptionData;
            profile.profileId = [[result objectForKey:@"data"] valueForKey:@"id"];
            [UserService sharedInstance].profile = profile;
            
            for (SubscriptionDataModel *subscription in [UserService sharedInstance].subscriptions) {
                if ([subscription.subscriptionId isEqualToString:profile.subscription.subscriptionId]) {
                    [[UserService sharedInstance] saveNewSubscription:subscription.subscriptionName];
                }
            }
            
            self.userEmail.text = [UserService sharedInstance].profile.email;
            
            NSString *subName = subscription.subscriptionName;
            if (!subName) {
                subName = @"1 Month";
            }
            [self setSubscriptionTypeLabel:subName];
             
            self.editBtn.enabled = YES;
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:profile.email forKey:@"user"];
            [defaults setValue:[[UserService sharedInstance] getUserSubscription] forKey:@"SubscriptionName"];
            [defaults setDouble:profile.subscription.subscriptionEndDate forKey:@"SubscriptionEndDate"];
            NSNumber *subscriptionEndDate = [NSNumber numberWithDouble:profile.subscription.subscriptionEndDate];
            [defaults setValue:[subscriptionEndDate stringValue] forKey:@"SubscriptionEndDateInString"];
            [defaults synchronize];
        } else {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            self.userEmail.text = [defaults objectForKey: @"user"];
            NSString *subName = [[UserService sharedInstance] getUserSubscription];
            if (!subName) {
                subName = @"1 Month";
            }
            [self setSubscriptionTypeLabel:subName];
            
            self.editBtn.enabled = YES;
            [self refreshToken];
        }
    }];
}

#pragma mark­ - Button actions

- (IBAction)editProfile:(id*)sender {
    self.editBtn.enabled = NO;
}

@end
