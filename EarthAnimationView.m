//
//  EarthAnimationView.m
//
//  Created by Igor Medelyan on 1/12/17.
//

#import "EarthAnimationView.h"
#import <QuartzCore/QuartzCore.h>

#define degreesToRadians(deg) (deg / 180.0 * M_PI)

@implementation EarthAnimationView

#pragma mark­ - Initialization

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    return self;
}

- (CGPoint)setPointToAngle:(int)angle center:(CGPoint)centerPoint radius:(double)radius {
    return CGPointMake(radius*cos(degreesToRadians(angle)) + centerPoint.x, radius*sin(degreesToRadians(angle)) + centerPoint.y);
}

#pragma mark - Drawing

- (void)clearAnimation {
    NSArray* views = self.subviews;
    [self.circle removeFromSuperlayer];
    for (UIView *v in views) {
        [v removeFromSuperview];
    }
}

- (void)drawAnimation {
    if (!_drawAnimationView) return;
    
    //Draw the animation of the circle
    int radius = 115;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.29 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.circle = [CAShapeLayer layer];
        self.circle.path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.frame.size.width/2,
                                                                        self.frame.size.height/2)
                                                     radius:radius
                                                 startAngle:M_PI*2.35
                                                   endAngle:M_PI*0.15
                                                  clockwise:NO].CGPath;
        self.circle.fillColor = [UIColor clearColor].CGColor;
        self.circle.strokeColor = [UIColor whiteColor].CGColor;
        self.circle.lineWidth = 1;
        [self.layer addSublayer:self.circle];

        CABasicAnimation *drawAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
        drawAnimation.duration            = 2.0;
        drawAnimation.repeatCount         = 1.0;
        drawAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
        drawAnimation.toValue   = [NSNumber numberWithFloat:1.0f];
        drawAnimation.removedOnCompletion = NO;
        [self.circle setMinificationFilter:kCAFilterTrilinear];
        [self.circle addAnimation:drawAnimation forKey:@"drawCircleAnimation"];
    });

    //Draw the animation of Casper - from button to circle
    self.casper = [[UIImageView alloc] initWithFrame:CGRectMake(50,50,20,20)];
    self.casper.image = [UIImage imageNamed:@"main_connect_button"];
    [self.casper.layer setMinificationFilter:kCAFilterTrilinear];
    [self addSubview:self.casper];
    
    int angle = 60; // start angle position 0-360 deg
    CGPoint center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    CGPoint startButton = CGPointMake(self.frame.size.width/2+30, self.frame.size.height-90);
    CGPoint startCircle = [self setPointToAngle:angle center:center radius:radius]; //point for start moving
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, startButton.x, startButton.y);
    double stepX = (startCircle.x-startButton.x)/70;
    double stepY = (startButton.y-startCircle.y)/70;
    CGPoint expPoint = startButton;
    for (int a =0; a<35; a++) //set path points from button to circle
    {
        expPoint.x = startButton.x + stepX;
        expPoint.y = startButton.y - stepY;
        startButton.x = expPoint.x + stepX;
        startButton.y = expPoint.y - stepY;
        CGPathAddQuadCurveToPoint(path, NULL,expPoint.x, expPoint.y, startButton.x, startButton.y);
    }
    
    CAKeyframeAnimation *pathAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    pathAnimation.removedOnCompletion = NO;
    pathAnimation.path = path;
    [pathAnimation setCalculationMode:kCAAnimationCubic];
    [pathAnimation setFillMode:kCAFillModeForwards];
    pathAnimation.duration = 0.29;
    [self.casper.layer addAnimation:pathAnimation forKey:nil];
    
    //Draw the animation of Casper - by the circle
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.29 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        int angle = 60; // start angle position
        CGPoint center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
        CGPoint startCircle = [self setPointToAngle:angle center:center radius:radius]; //point for start moving
        CGMutablePathRef path = CGPathCreateMutable();
        CGPathMoveToPoint(path, NULL, startCircle.x, startCircle.y);
        CGPoint expPoint = startCircle;
        
        for (int a=0; a<180; a++) //set path points form angle
        {
            angle-=1;
            expPoint = [self setPointToAngle:angle center:center radius:radius];
            angle-=1;
            startCircle = [self setPointToAngle:angle center:center radius:radius];
            CGPathAddQuadCurveToPoint(path, NULL,expPoint.x, expPoint.y, startCircle.x, startCircle.y);
        }
        
        CAKeyframeAnimation *pathAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
        pathAnimation.removedOnCompletion = NO;
        pathAnimation.path = path;
        [pathAnimation setCalculationMode:kCAAnimationCubic];
        [pathAnimation setFillMode:kCAFillModeForwards];
        pathAnimation.duration = 1.8;
        pathAnimation.repeatCount = INFINITY;
        [self.casper.layer addAnimation:pathAnimation forKey:nil];
    });
    
    //Draw the animation of Casper - resize animation
    CABasicAnimation *resizeAnimation = [CABasicAnimation animationWithKeyPath:@"bounds.size"];
    [resizeAnimation setFromValue:[NSValue valueWithCGSize:CGSizeMake(60.0f, 60.0f)]];
    resizeAnimation.fillMode = kCAFillModeForwards;
    resizeAnimation.duration = 2.0;
    resizeAnimation.removedOnCompletion = NO;
    [self.casper.layer addAnimation:resizeAnimation forKey:nil];
}

- (void)hideCasper {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.8 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.casper.hidden = YES;
    });

    CABasicAnimation *opacityAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    opacityAnimation.fromValue = [NSNumber numberWithFloat:1.0];
    opacityAnimation.toValue = [NSNumber numberWithFloat:0.0];
    opacityAnimation.duration = 0.9;
    opacityAnimation.repeatCount = 1;
    opacityAnimation.removedOnCompletion = NO;
    [self.casper.layer addAnimation:opacityAnimation forKey:@"opacityAnimation"];
}

- (void)flashCircle {
    CABasicAnimation *flashAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    flashAnimation.fromValue = [NSNumber numberWithFloat:1.0];
    flashAnimation.toValue = [NSNumber numberWithFloat:0.0];
    flashAnimation.duration = 0.3;
    flashAnimation.repeatCount = 3;
    flashAnimation.removedOnCompletion = NO;
    [self.circle addAnimation:flashAnimation forKey:@"flashAnimation"];
}

- (void)showEarthAnimation {
    self.hidden = NO;
    self.casper.hidden = NO;
    self.drawAnimationView = YES;
    [self drawAnimation];
}

@end
