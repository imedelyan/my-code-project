//
//  ConnectButton.h
//
//  Created by Igor Medelyan on 5/8/17.
//

#import <UIKit/UIKit.h>

@interface ConnectButton : UIButton

- (UIColor *)colorFromHexString:(NSString *)hexString;
- (void)flashConnectButton;
- (void)showConnectButtonRotationAnimation;
- (void)removeConnectButtonRotationAnimation;

@end
